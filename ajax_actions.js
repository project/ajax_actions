/*
* Various javascript fixes for embedded forms
*/

(function ($) {
  Drupal.ajaxActions = Drupal.ajaxActions || {};
  
  Drupal.ajaxActions.clickAjaxLink = function() {
    if ($(this).hasClass('ajax-actions-ajaxing')) {
      return false;
    }

    var data = $(this).data('ajax_actions');
    var url = $(this).attr('href');
    var object = $(this);
    $(this).addClass('ajax-actions-ajaxing');
    try {
      $.ajax({
        type: "POST",
        url: url,
        data: {'ajax_actions' : data},
        global: true,
        success: Drupal.CTools.AJAX.respond,
        error: function(xhr) {
          Drupal.CTools.AJAX.handleErrors(xhr, url);
        },
        complete: function() {
          $('.ajax-actions-ajaxing').removeClass('ajax-actions-ajaxing');
        },
        dataType: 'json'
      });
    }
    catch (err) {
      alert("An error occurred while attempting to process " + url);
      $('.ajax-actions-ajaxing').removeClass('ajax-actions-ajaxing');
      return false;
    }

    return false;
  };

  /**
   * Additional responder commands for ctools.
   */
  
  Drupal.CTools.AJAX.commands.attach_behaviors = function(data) {
    Drupal.attachBehaviors($(data.selector));
  }
  
  Drupal.CTools.AJAX.commands.add_class = function(data) {
    $(data.selector).addClass(data.class);
    Drupal.attachBehaviors($(data.selector));
  }

  Drupal.CTools.AJAX.commands.remove_class = function(data) {
    $(data.selector).removeClass(data.class);
    Drupal.attachBehaviors($(data.selector));
  }
  
  Drupal.CTools.AJAX.commands.ajax_submit = function(data) {
    $(data.selector).submit();
  }

  Drupal.CTools.AJAX.commands.empty = function(data) {
    $(data.selector).empty();
  }
  
  /**
   * Bind links that will open modals to the appropriate function.
   */
  Drupal.behaviors.ajaxActions = function(context) {
    
    // Bind links
    $('a.ajax-actions-link', context)
      .not('a.ajax-actions-link-processed')
      .addClass('ajax-actions-link-processed')
      .click(Drupal.ajaxActions.clickAjaxLink);

  };
})(jQuery);